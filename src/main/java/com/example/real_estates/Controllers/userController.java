package com.example.real_estates.Controllers;

import com.example.real_estates.Models.UserModel;
import com.example.real_estates.Services.userService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class userController {

    @Autowired
    private userService userService;

    @GetMapping("/user")
    public List<UserModel> getAll(){
        return userService.index();
    }

    @PostMapping("createUser")
    public void create(@RequestBody Map<String, String> body){
        userService.create(body);
    }

    @GetMapping("/user/{id}")
    public UserModel findOne(@PathVariable String id){
        return userService.findOne(id);
    }

    @PatchMapping("/user/{id}")
    public UserModel update(@PathVariable String id,@RequestBody Map<String, String> body){
       return userService.update(id,body);
    }

    @DeleteMapping("/user/{id}")
    public void delete(@PathVariable String id){
        userService.delete(id);
    }
}
