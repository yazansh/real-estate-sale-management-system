package com.example.real_estates.Controllers;

import com.example.real_estates.Models.EstateProjection;
import com.example.real_estates.Models.Estates;
import com.example.real_estates.Services.estatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class estatesController {

    @Autowired
    private estatesService estatesService;

    @GetMapping("/estates")
    public List<EstateProjection> getAll(@RequestParam("page") int page){
        return estatesService.index(page);
    }

    @PostMapping("estates")
    public EstateProjection create(@RequestBody Map<String, String> body){
       return estatesService.create(body);
//        return ResponseEntity.status(HttpStatus.CREATED)
//                .body("Your estates created" );
    }

    @GetMapping("/estates/{id}")
    public Estates findOne(@PathVariable String id){
        return estatesService.findOne(id);
    }

    @PutMapping("/saleEstates/{id}")
    public Estates saleEstates(@RequestBody Map<String, String> body,@PathVariable String id){
        return this.estatesService.saleEstates(id,body);
    }
    @PutMapping("/editEstates/{id}")
    public Estates editEstates(@RequestBody Map<String, String> body,@PathVariable String id){
        return this.estatesService.editEstates(id,body);
    }

    @GetMapping("/estatesNotSold")
    public List<EstateProjection> estatesNotSold(@RequestParam("page") int page){
        return this.estatesService.estatesNotSold(page);
    }

    @GetMapping("/estatesByName/{name}")
    public Estates estatesByName(@PathVariable String name){
        return this.estatesService.getByName(name);
    }

    @DeleteMapping("/estates/{id}")
    public void delete(@PathVariable String id){
        estatesService.delete(id);
    }
}
