package com.example.real_estates.Controllers;

import com.example.real_estates.Models.Parameter;
import com.example.real_estates.Services.parameterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class ParameterController {

    @Autowired
    private parameterService parameterService;

    @GetMapping("/parameter")
    public List<Parameter> getAll(){
        return parameterService.index();
    }

    @PostMapping("parameter")
    public ResponseEntity<String> create(@RequestBody Map<String, String> body){
        parameterService.create(body);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body("Your parameter created" );
    }

    @GetMapping("/parameter/{myKey}")
    public Parameter findOne(@PathVariable String myKey){
        return parameterService.findOne(myKey);
    }

    @DeleteMapping("/parameter/{myKey}")
    public void delete(@PathVariable String myKey){
        parameterService.delete(myKey);
    }
}
