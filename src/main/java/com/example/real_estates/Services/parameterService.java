package com.example.real_estates.Services;

import com.example.real_estates.Models.Parameter;
import com.example.real_estates.Repositories.parameterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.lang.Double.parseDouble;

@Service
public class parameterService {

    @Autowired
    private parameterRepository parameterRepository;

    @Cacheable(value="allParameter")
    public List<Parameter> index(){

        return this.parameterRepository.findAll();
    }

    public void create(Map<String, String> body){
        String myKey = body.get("myKey");
        double value = parseDouble(body.get("value"));
        Parameter parameter = new Parameter(myKey,value);
        this.parameterRepository.save(parameter);
    }

    @Cacheable(value="parameter")
    public Parameter findOne(String myKey){

    //    Optional<Parameter> optional = this.parameterRepository.findByIkey(myKey).get(0);
        return this.parameterRepository.findByMyKey(myKey).get(0);
    }



    public void delete (String id){
        int intId = Integer.parseInt(id);
        this.parameterRepository.deleteById(intId);
    }


}
