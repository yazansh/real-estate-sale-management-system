package com.example.real_estates.Services;

import com.example.real_estates.Aspects.MyUpdateLogger;
import com.example.real_estates.Models.UserModel;
import com.example.real_estates.Repositories.userRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class userService {

    @Autowired
    private userRepository userRepository;



    public List<UserModel> index(){
        return this.userRepository.findAll();
    }

    public void create(Map<String, String> body){
        String username = body.get("username");
        String email = body.get("email");
        String password = body.get("password");
        String phone = body.get("phone");
        UserModel user = new UserModel(username,email,password,phone);
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        this.userRepository.save(user);
    }

    public UserModel findOne(String id){
        int intId = Integer.parseInt(id);
        Optional<UserModel> optional = this.userRepository.findById(intId);
        return optional.get();
    }
    @MyUpdateLogger
    public UserModel update(String id,Map<String, String> body){
        int intId = Integer.parseInt(id);
        Optional<UserModel> optional = this.userRepository.findById(intId);
        if(body.get("username")!=null)
            optional.get().setUsername(body.get("username"));
        if (body.get("email")!=null)
            optional.get().setUsername(body.get("email"));
        if (body.get("phone")!=null)
            optional.get().setUsername(body.get("phone"));
        this.userRepository.save(optional.get());
        return optional.get();
    }
    public  UserModel findUserByName(String username){
        return this.userRepository.findUserByUsername(username);
    }

    public void delete (String id){
        int intId = Integer.parseInt(id);
        this.userRepository.deleteById(intId);
    }


}
