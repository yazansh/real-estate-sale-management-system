package com.example.real_estates.Services;

import com.example.real_estates.Aspects.MyUpdateLogger;
import com.example.real_estates.Models.EstateProjection;
import com.example.real_estates.Models.Estates;
import com.example.real_estates.Repositories.estatesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.*;

import static java.lang.Double.parseDouble;

@Service
public class estatesService {

    @Autowired
    private estatesRepository estatesRepository;

    @Autowired
    private parameterService parameterService;

    public List<EstateProjection> index(int page){
        Pageable pageable = PageRequest.of(page, 10);
        List<EstateProjection> estateProjectionList = estatesRepository.getEstatesByIdNotNull(EstateProjection.class,pageable);
        calculateDefaultSellingPrice(estateProjectionList);
        return estateProjectionList;
    }

    public EstateProjection create(Map<String, String> body){
        String name = body.get("name");
        double price = parseDouble(body.get("price"));
        int shares;
        if (body.get("shares") != null){
            shares = Integer.parseInt(body.get("shares"));
        } else {
            shares = (int) parameterService.findOne("shares").getValue();
        }
        Estates estates = new Estates(name,price,shares);
        this.estatesRepository.save(estates);
        List<EstateProjection> estateProjection =
                estatesRepository.getById(EstateProjection.class,estates.getId());
        calculateDefaultSellingPrice(estateProjection);
        return estateProjection.get(0);
    }

    public Estates findOne(String id){
        int intId = Integer.parseInt(id);
        Optional<Estates> optional = this.estatesRepository.findById(intId);
        return optional.get();
    }
    @MyUpdateLogger
    public Estates saleEstates(String id,Map<String, String> body){
        int intId = Integer.parseInt(id);
        Optional<Estates> optional = this.estatesRepository.findById(intId);
        optional.get().setSaleDate(LocalDateTime.now());
        optional.get().setSalePrice(parseDouble(body.get("sale_price")));
        optional.get().setSellerName(body.get("seller_name"));
        return this.estatesRepository.save(optional.get());

    }
    @MyUpdateLogger
    public Estates editEstates(String id,Map<String, String> body){
        int intId = Integer.parseInt(id);
        Optional<Estates> optional = this.estatesRepository.findById(intId);
        optional.get().setName(body.get("name"));
        optional.get().setShares(Integer.parseInt(body.get("shares")));
        optional.get().setPrice(parseDouble(body.get("price")));
        return this.estatesRepository.save(optional.get());
    }
    public List<EstateProjection> estatesNotSold(int page){
        Pageable pageable = PageRequest.of(page, 10);
        List<EstateProjection> estateProjectionList = estatesRepository.getEstatesBySellerNameNull(EstateProjection.class,pageable);
        calculateDefaultSellingPrice(estateProjectionList);
        return estateProjectionList;
    }
    private  void calculateDefaultSellingPrice (List<EstateProjection> estateProjectionList){
        for (EstateProjection estateProjection : estateProjectionList) {
            double defaultSellingPrice = estateProjection.getPrice() * parameterService.findOne("profitRate").getValue();
            estateProjection.setDefaultSellingPrice(defaultSellingPrice);
        }
    }
    public Estates getByName(String name){
        List<Estates> estatesList = estatesRepository.findByName(name);
        if (estatesList.size()>0){
            return this.estatesRepository.findByName(name).get(0);
        }
        throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "entity not found"
        );
    }
    public void delete (String id){
        int intId = Integer.parseInt(id);
        this.estatesRepository.deleteById(intId);
    }
}
