package com.example.real_estates.Services;

import com.example.real_estates.Models.LogModel;
import com.example.real_estates.Repositories.LogRepository;
import com.example.real_estates.Repositories.userRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class LogService {
    @Autowired
    private LogRepository logRepository;
    public void logCreate (String operation, String user, Date date){
        LogModel logModel = new LogModel(operation,user,date);
        this.logRepository.save(logModel);
    }
}
