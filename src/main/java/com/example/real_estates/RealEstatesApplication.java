package com.example.real_estates;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.web.bind.annotation.RestController;

//@SpringBootApplication
@RestController
@EnableCaching
@SpringBootApplication(exclude = RedisAutoConfiguration.class)
public class RealEstatesApplication {


    static Logger logger = LoggerFactory.getLogger(RealEstatesApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(RealEstatesApplication.class, args);
    }

}
