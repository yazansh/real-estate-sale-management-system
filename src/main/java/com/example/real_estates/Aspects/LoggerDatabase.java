package com.example.real_estates.Aspects;

import com.example.real_estates.Services.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
@Component
public class LoggerDatabase {

    @Autowired
    private LogService logService;
    void writeToDatabase (String operation,String user){
        Date date = new Date();
        logService.logCreate(operation,user,date);
    }
}
