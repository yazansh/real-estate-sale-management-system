package com.example.real_estates.Aspects;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAspect {
    @Autowired
    LoggerDatabase loggerDatabase;
    @After("@annotation(com.example.real_estates.Aspects.MyUpdateLogger)")
    public void logUpdate() {
        loggerDatabase.writeToDatabase("update",SecurityContextHolder.getContext().getAuthentication().getName());

    }
    @After("execution(*  com.example.real_estates.Services.*.create(..))")
    public void logCreate() {
        loggerDatabase.writeToDatabase("create",SecurityContextHolder.getContext().getAuthentication().getName());
    }
    @After("execution(*  com.example.real_estates.Services.*.delete(..))")
    public void logDelete() {
        loggerDatabase.writeToDatabase("delete",SecurityContextHolder.getContext().getAuthentication().getName());
    }

}
