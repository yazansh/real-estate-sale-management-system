package com.example.real_estates.Models;

import org.apache.commons.logging.Log;

import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;
@Entity
public class LogModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    String operation;
    String user;
    Date date;
    public LogModel(String operation, String user, Date date) {

        this.operation = operation;
        this.user = user;
        this.date = date;
    }
    LogModel(){

    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
