package com.example.real_estates.Models;

import javax.persistence.Column;
import java.time.LocalDateTime;

public class EstateProjection {
     int id;
    String name;
    int shares;
    double price;

     LocalDateTime saleDate;
     double salePrice;
     String sellerName;
    double defaultSellingPrice;
    public EstateProjection(int id,String name, int shares, double price,  LocalDateTime saleDate, double salePrice, String sellerName) {
        this.id = id;
        this.name = name;
        this.shares = shares;
        this.price = price;
        this.saleDate = saleDate;
        this.salePrice = salePrice;
        this.sellerName = sellerName;
    }

    public int getId() {
        return id;
    }

    public LocalDateTime getSaleDate() {
        return saleDate;
    }

    public double getSalePrice() {
        return salePrice;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setDefaultSellingPrice(double defaultSellingPrice) {
        this.defaultSellingPrice = defaultSellingPrice;
    }

    public String getName() {
        return name;
    }

    public int getShares() {
        return shares;
    }

    public double getPrice() {
        return price;
    }

    public double getDefaultSellingPrice() {
        return defaultSellingPrice;
    }
}
