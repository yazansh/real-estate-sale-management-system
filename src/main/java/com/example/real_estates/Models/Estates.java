package com.example.real_estates.Models;

import com.example.real_estates.Audit.AuditModel;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@DynamicUpdate
@EntityListeners(AuditingEntityListener.class)

public class Estates extends AuditModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String name;
    @Column
    private double price;
    @Column
    private int shares;
    @Column
    private LocalDateTime saleDate;
    @Column
    private double salePrice;
    @Column
    private String sellerName;

    public Estates(String name, double price, double salePrice, String sellerNAme,int shares) {
        this.name = name;
        this.price = price;
        this.shares = shares;
        this.saleDate = null;
        this.salePrice = salePrice;
        this.sellerName = sellerNAme;
    }
    public Estates (String name,double price,int shares){
        this.name = name;
        this.price = price;
        this.shares = shares;
    }
    public Estates() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getShares() {
        return shares;
    }

    public void setShares(int shares) {
        this.shares = shares;
    }



    public double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(double salePrice) {
        this.salePrice = salePrice;
    }

    public LocalDateTime getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(LocalDateTime saleDate) {
        this.saleDate = saleDate;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }
}
