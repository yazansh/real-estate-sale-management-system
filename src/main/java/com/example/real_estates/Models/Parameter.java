package com.example.real_estates.Models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Parameter implements Serializable {
    private static final long serialVersionUID = 7156526077883281623L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String myKey;
    @Column
    private double value;


    public Parameter(String myKey, double value) {
        this.myKey = myKey;
        this.value = value;
    }

    public Parameter() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return myKey;
    }

    public void setKey(String myKey) {
        this.myKey = myKey;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
