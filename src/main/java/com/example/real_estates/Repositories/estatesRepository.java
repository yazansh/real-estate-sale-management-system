package com.example.real_estates.Repositories;

import com.example.real_estates.Models.EstateProjection;
import com.example.real_estates.Models.Estates;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface estatesRepository extends PagingAndSortingRepository<Estates,Integer> {

    <T> List<T> getEstatesByIdNotNull(Class<T> type, Pageable pageable);
    List<Estates> getEstatesBySellerNameNull();
    List<Estates> findByName(String name);
    <T> List<T> getEstatesBySellerNameNull(Class<T> type, Pageable pageable);
    <T> List<T>   getById(Class<T> type,int id);
}
