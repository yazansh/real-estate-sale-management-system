package com.example.real_estates.Repositories;

import com.example.real_estates.Models.Estates;
import com.example.real_estates.Models.LogModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogRepository extends JpaRepository<LogModel,Integer> {

}
