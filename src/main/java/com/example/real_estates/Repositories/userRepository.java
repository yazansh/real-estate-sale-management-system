package com.example.real_estates.Repositories;

import com.example.real_estates.Models.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface userRepository extends JpaRepository<UserModel,Integer> {
    UserModel  findUserByUsername(String username);

}
