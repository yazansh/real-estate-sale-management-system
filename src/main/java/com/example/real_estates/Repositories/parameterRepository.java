package com.example.real_estates.Repositories;

import com.example.real_estates.Models.Estates;
import com.example.real_estates.Models.Parameter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface parameterRepository extends JpaRepository<Parameter,Integer> {
    List<Parameter> findByMyKey(String myKey);
}
